package wtf.scala.course.tictactoe

import scalikejdbc.DB.futureLocalTx
import scalikejdbc.{ConnectionPool, _}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object tttDb {
  def init(): Unit = {
    Class.forName("org.h2.Driver")
    ConnectionPool.singleton("jdbc:h2:mem:teapot;DB_CLOSE_DELAY=-1", "", "")
  }

  def initDb(): Future[Boolean] = {
    DB futureLocalTx({ implicit session =>
          sql"""
          create table games (
            id serial not null primary key,
            next_step varchar (20) not null,
            won varchar(20),
            finished boolean,
            steps int,
            size varchar(20),
            crosses_length_to_win int,
            field clob)
          """.execute().apply()

          sql"""
          create table users (
            username varchar(20) not null primary key,
            password varchar (20) not null)
          """.execute().apply()

          sql"""
          create table gamesessions (
            uuid uuid not null primary key,
            username varchar (20) not null,
            timesession TIMESTAMP not null)
          """.execute().apply()

          Game.create(Game(1, "eagle"))
        })
  }

  def deleteAll(): Boolean = {
    DB autoCommit ({ implicit session =>
      sql"""
            DELETE FROM games
          """.execute().apply()
    })
    DB autoCommit ({ implicit session =>
      sql"""
            DELETE FROM users
          """.execute().apply()
    })

    DB autoCommit ({ implicit session =>
      sql"""
            DELETE FROM gamesessions
          """.execute().apply()
    })

  }
}
