package wtf.scala.course.tictactoe

import scalikejdbc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

// final case class UserLogin(username: String, password: String)
final case class User(username: String, password: String)

object User extends SQLSyntaxSupport[User] {
  override val tableName = "users"

  def apply(r: ResultName[User])(rs: WrappedResultSet) =
    new User(
      rs.string(r.username),
      rs.string(r.password)
    )

  def create(user: User)(implicit session: DBSession = AutoSession): Future[Boolean] = {
    val sql = withSQL(insert.into(User).namedValues(
      column.username -> user.username,
      column.password -> user.password.hashCode().toString()
    ))
    Future {
      sql.update().apply() == 1
    }
  }

  private val u = syntax

  def findByName(username: String)(implicit session: DBSession = AutoSession): Future[Option[User]] = Future {
    val sql = withSQL(
      select.from[User](User as u).where.eq(u.username, username))
    sql.map(User(u.resultName)).headOption().apply()
  }

  def  findFirstByName(username: String): Boolean = {
    val name: Option[String] = DB readOnly { implicit session =>
      SQL("select * from users where username = ?").bind(username).map(rs => rs.string("username")).first().apply()
    }
    name != None
  }
}
