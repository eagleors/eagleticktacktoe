package wtf.scala.course.tictactoe

import scalikejdbc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import java.util.UUID.randomUUID
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.Period
import java.util.Calendar

import scala.concurrent.duration._


final case class Gsession (username: String, uuid: String, timesession: Timestamp)


object Gsession extends SQLSyntaxSupport[Gsession]{
  override val  tableName = "gamesessions"
  val sessionTime: Duration = 100 seconds

  def apply(r: ResultName[Gsession])(rs : WrappedResultSet) =
    new Gsession(
      rs.string(r.username),
      rs.string(r.uuid),
      rs.timestamp(r.timesession)
    )

  // 1. Поискать существующую сессию этого пользователя, нет перейти на шаг 4
  // 2. Проверить, что время жизни нормальное, то обновить время сессии, нет перейти на шаг 3
  // 3. Удалить найденую сессию, а может лучше и все.
  def make(username: String)(implicit session: DBSession = AutoSession): Future[Boolean] = Future {
      ???
//    match(findByUserName(username)) {
//      case Some(gsession) => 1 == 1
//      case None => 1 == 2
 //   }
  }

    // 4. Создать новую сеcсию
  def create(username: String)(implicit session: DBSession = AutoSession): Future[Boolean] = {
    val gsession: Gsession = Gsession(username, randomUUID().toString, getCurrentdateTimeStamp)

    val sql = withSQL(insert.into(Gsession).namedValues(
      column.username -> gsession.username,
      column.uuid -> gsession.uuid,
      column.timesession -> gsession.timesession

    ))
    Future{
      sql.update().apply() == 1
    }
  }

  private val s = syntax

  def findByUid(uuid: String)(implicit session: DBSession = AutoSession): Future[Option[Gsession]] = Future{
    val sql = withSQL(
      select.from[Gsession](Gsession as s).where.eq(s.uuid, uuid))
    sql.map(Gsession(s.resultName)).headOption().apply()
  }

  def findByUserName(username: String)(implicit session: DBSession = AutoSession): Future[Option[Gsession]] = Future {
    val sql = withSQL(
      select.from[Gsession](Gsession as s).where.eq(s.username, username))
    sql.map(Gsession(s.resultName)).headOption().apply()
  }

  def getCurrentdateTimeStamp: Timestamp ={
    val today:java.util.Date = Calendar.getInstance.getTime
    val timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val now:String = timeFormat.format(today)
    val re = java.sql.Timestamp.valueOf(now)
    re
  }
}
