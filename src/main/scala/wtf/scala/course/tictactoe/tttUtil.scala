package wtf.scala.course.tictactoe

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directive0
import akka.http.scaladsl.server.Directives._
import scalikejdbc.DB
import spray.json._

import scala.concurrent.ExecutionContext.Implicits.global

trait WithAuth {
  def withAuth: Directive0 = optionalHeaderValueByName("admin").flatMap {
    case Some(k) => pass
    case _ => complete(StatusCodes.Unauthorized)
  }
}

trait tttUtil extends SprayJsonSupport with DefaultJsonProtocol with WithAuth {
  implicit val gameFormat = jsonFormat2(Game.apply)
  implicit val userFormat = jsonFormat2(User.apply)

  val tttRepository: TttRepository
  val route = {
    path("game" / LongNumber) { id =>
      get {
        parameterMap { paramsMap =>
          onSuccess(tttRepository.getGame(id)) {
            case Some(game) =>
              complete(StatusCodes.OK -> JsObject(game.toJson.asJsObject.fields ++ Map("params" -> paramsMap.toJson)))
            case None =>
              complete(StatusCodes.NotFound)
          }
        }
      } ~ post {
        entity(as[Game]) { game =>
          complete {
            DB futureLocalTx { implicit session =>
              tttRepository.createGame(game).map(_ => StatusCodes.OK)
            }
          }
        }
      }
    } ~ path("user") {
      post {
        entity(as[User]) { user =>
          if (user.username.length < 3 || user.username.length > 20 || user.password.length < 8 || user.password.length > 100)
            complete(StatusCodes.BadRequest -> "имя должно быть от 4 до 20 символов, а пароль от 8 до 100")
          else if (User.findFirstByName(user.username)) // а как здесь со future работать, ведь пользователь все равно ждет?
            complete(StatusCodes.Conflict -> "есть уже такой пользователь")
          else
            complete {

              DB futureLocalTx { implicit session =>
                tttRepository.createUser(user).map(_ => StatusCodes.OK -> JsObject(user.toJson.asJsObject.fields))
              }
            }
        }
      }
    } ~ path("user" /  Segment){ username =>
    get {
        parameterMap { paramsMap =>
          onSuccess(tttRepository.getUser(username)) {
            case Some(user) =>
              complete(StatusCodes.OK -> JsObject(user.toJson.asJsObject.fields ++ Map("params" -> paramsMap.toJson)))
            case None =>
              complete(StatusCodes.NotFound)
          }
        }

      }
    } ~ path("user" / "login") {
         post {
           entity(as[User]) { userInput =>
             onSuccess(tttRepository.getUser(userInput.username)) {
               case Some(user) =>
                 if (userInput.password.hashCode.toString() == user.password) {
                   println("good")
                   complete(StatusCodes.OK)
                 }
                 else {
                   println("bad")
                   complete(StatusCodes.Forbidden)
                 }
               case None =>
                 complete(StatusCodes.NotFound)
             }
           }
         }
    } ~ path("debug" / "reset"){
            withAuth{
              post {
                val b = tttRepository.reset()
                complete{ StatusCodes.OK }
              }
        }
    }
  }
}