package wtf.scala.course.tictactoe


import scalikejdbc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


final case class Game (id: Long, next_step: String)


object Game extends SQLSyntaxSupport[Game]{
  override val  tableName = "games"

  def apply(r: ResultName[Game])(rs : WrappedResultSet) =
    new Game(
      rs.long(r.id),
      rs.string(r.next_step)
    )


  def create(game: Game)(implicit session: DBSession = AutoSession): Future[Boolean] ={
    val sql = withSQL(insert.into(Game).namedValues(
      column.id -> game.id,
      column.next_step -> game.next_step
    ))
    Future{
      sql.update().apply() == 1
    }
  }

  private val g = syntax

  def findById(id: Long)(implicit session: DBSession = AutoSession): Future[Option[Game]] = Future{
    val sql = withSQL(
      select.from[Game](Game as g).where.eq(g.id, id))
    sql.map(Game(g.resultName)).headOption().apply()
  }

  private  val gs = Game.syntax
  def getList(id: Long, offset: Long)(implicit session: DBSession = AutoSession): Future[List[Game]] = Future {
    val games = withSQL
      { select.from(Game as gs).where.ge(gs.id, offset).limit(id.toInt) }.map(Game(gs.resultName)).list().apply()
    games
  }
}
