package wtf.scala.course.tictactoe

import scalikejdbc.DB

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

final case class StepReply(codeError: String, game: Option[Game])
final case class LogoutReply(codeError: String, game: Option[Game])

trait TttRepository {
  // игры
  def createGame(game: Game): Future[Unit]
  def getGame(id: Long): Future[Option[Game]]
  def getGames(id: Long, offset: Long): Future[List[Game]] // может ли быть без параметров?

  def gameStep(id: Long, step: List[Int]): StepReply

    // пользователи - игроки
  def createUser(user: User): Future[Unit]
  def getUser(username: String): Future[Option[User]]
  def getUsers(id: Long, offset: Long): Future[Option[List[User]]] // может ли быть без параметров?
  def userLogin(username: String, password: String): String // Если логин и пароль неверны
  def userLogout(sessionuid: String): String

  // отладка
  def reset(): Boolean

}

object DBTttRepository extends TttRepository {
  tttDb.init()
  Await.result(tttDb.initDb(), Duration.Inf)

  override def createGame(game: Game): Future[Unit] = DB.futureLocalTx(implicit session => Game.create(game).map(_ => ()))
  override def getGame(id: Long): Future[Option[Game]] = DB.futureLocalTx(implicit session => Game.findById(id))
  override def getGames(id: Long,offset: Long): Future[List[Game]] = DB.futureLocalTx(implicit session => Game.getList(id, offset))

  override def gameStep(id: Long, step: List[Int]): StepReply = ???

  override def createUser(user: User): Future[Unit] = DB.futureLocalTx(implicit session => User.create(user).map(_ => ()))
  override def getUser(username: String): Future[Option[User]] = DB.futureLocalTx(implicit sesssion => User.findByName(username))
  override def getUsers(id: Long, offset: Long): Future[Option[List[User]]] = ???
  override def userLogin(username: String, password: String): String = ???
  override def userLogout(sessionuid: String): String = ???

  override def reset(): Boolean = DB.localTx(implicit session => tttDb.deleteAll())
}
